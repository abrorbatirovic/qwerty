const index = props => {
  const { children, className } = props

  return (
    <label {...props} className={`${className} label`}>
      {children}
    </label>
  )
}

export default index
