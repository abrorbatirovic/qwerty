import React ,{useState} from 'react';

import {Base64} from "js-base64";


import './main.css'
import avatar from '../../img/customer-1 1.png'
import {useNavigate} from "react-router-dom";


const Index = () => {

    const history = useNavigate()

    const [user, setUser] = useState(localStorage.getItem('Authority') ? JSON.parse(Base64.decode(localStorage.getItem('Authority'))) : "")
    const [drop, setDrop] = useState(false)




    return (
        <>
            <div className={'navbar'}>
                {
                    drop && <div className={'dropdown'}>
                        <div onClick={()=>history('/')}>Log Out</div>
                    </div>
                }
                <div className={'navbar-user-avatar'}>
                    <div>
                        <h3>{user?.fullName}</h3>
                    </div>
                    <div>
                        <i className="fa-regular fa-bell"></i>
                        <span/>
                    </div>
                    <div onClick={()=>setDrop(p=>!p)}>
                        <img src={avatar} alt='img not found' />
                    </div>

                </div>
            </div>
            
        </>
    );
}

export default Index;