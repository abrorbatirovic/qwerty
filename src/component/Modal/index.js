import React from 'react';

const Index = () => {

    const onFinish = (e) => {
        e.preventDefault()
    }

    return (
        <div>
            <form onSubmit={onFinish} id={'form-update'}>
                <label htmlFor="i-1" >
                    Phone Number
                </label>
                <input type="text" id='i-1'/>

                <label htmlFor="i-2">
                    Password
                </label>
                <input type="text" id={'i-2'}/>

                <label htmlFor="i-3">
                    Full Name
                </label>
                <input type="text" id={'i-3'}/>

                <label htmlFor="i-4">
                   Role
                </label>
                <input type="text" id={'i-4'}/>
            </form>
            <div>
                <button type={'submit'} form={'form-update'} >Kirish</button>
            </div>
        </div>
    );
};

export default Index;