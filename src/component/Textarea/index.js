import { forwardRef } from 'react'

const index = forwardRef((props, ref) => <textarea ref={ref} {...props} />)

export default index
