import { forwardRef } from "react"

const index = forwardRef((props, ref) => <input ref={ref} {...props} />)

export default index
