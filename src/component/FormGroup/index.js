const index = props => <div {...props}>{props.children}</div>

export default index
