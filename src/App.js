import React from "react";
import Admin from "./view/admin";
import {Route, Routes} from "react-router-dom";
import Category from "./view/category/Category";
import CategoryDetails from "./view/category/CategoryDetails";
import Car from "./view/car/Car";
import Form from './view/Form'
import Login from './view/login/Login'

function App() {
    return (
        <div className="">
            <Routes>
                <Route path='/' element={<Category />} />
                {/*<Route exact path={"/category"} element={<Category/>}/>*/}
                <Route exact path={"/category/:categoryId"} element={<CategoryDetails/>}/>
                <Route exact path={"/car/:id"} element={<Car/>}/>
                <Route path={"/login"} element={<Login/>}/>
                <Route path={"/admin/:a"} element={<Admin/>}/>
                <Route path={"*"} element={Category} />
            </Routes>
        </div>
    );
}

export default App;
