import {createSlice} from "@reduxjs/toolkit";
import {employeeLogin, employeeAccount, apiCall} from '../config/config'

import {Base64} from "js-base64";


const auth = createSlice({
    name: 'auth',
    initialState: {
        loading: false,
        data: [],
        status: '',
        userStatus: ''
    },
    reducers: {
        onStart: (state) => {
            state.loading = true
            state.status = ''
            state.userStatus = ''
        },
        clearAuth:(state,action)=>{
            state.loading = false
            state.status = ''
            state.userStatus = ''
        },
        onSuccess: (state, action) => {
            localStorage.setItem('posToken', Base64.encode(action.payload.data.token));
            state.loading = false
            state.status = action.payload
        },
        getProfileData: (state, action) => {
            localStorage.setItem('Authority', Base64.encode(JSON.stringify((action.payload.data))))
            state.userStatus = action.payload
        },
        onFail: (state, action) => {
            state.loading = false
        }
    },

})

export const loginStart = (data) => apiCall({
    url: employeeLogin,
    method: 'post',
    data,
    onStart: auth.actions.onStart.type,
    onSuccess: auth.actions.onSuccess.type,
    onFail: auth.actions.onFail.type,
});

export const getProfile = () => apiCall({
    url: employeeAccount,
    method: 'get',
    onStart: auth.actions.onStart.type,
    onSuccess: auth.actions.getProfileData.type,
    onFail: auth.actions.onFail.type
});


export default auth.reducer
export const {clearAuth} = auth.actions