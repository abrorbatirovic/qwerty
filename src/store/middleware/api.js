import axios from "axios";
import {Base64} from "js-base64";

const api = ({dispatch}) => (next) => (action) => {

    if (action.type !== 'apiCall') {
        next(action)
        return
    }

    next(action)

    const token = localStorage.getItem('posToken')

    const {url, method, params, data, onStart, onSuccess, onFail} = action.payload

    const authorization = token ? {"Authorization": `Bearer ${Base64.decode(token)}`} : null

    dispatch({type: onStart})

    axios({
        baseURL: 'https://cartestwebapp.herokuapp.com/',
        url,
        method,
        data,
        params,
        headers: authorization
    }).then(res => {
        dispatch({
            type: onSuccess,
            payload: res.data
        })
    }).catch(e => {
        dispatch({
            type: onFail,
            payload: e
        })
    })

}

export default api