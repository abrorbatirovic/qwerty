import {createSlice} from '@reduxjs/toolkit'
import {
    apiCall,
    apiCar
} from "../config/config";

const car = createSlice({
    name: 'car',
    initialState: {
        isLoading: false,
        loading: false,
        data: [],
        oneCar: {},
        status: '',
        pageCount: 0,
    },
    reducers: {
        onGetStart: (state) => {
            state.loading = false
            state.isLoading = true
            state.data = []
        },
        onAddStart: (state) => {
            state.isLoading = false
            state.loading = true
        },
        onFail: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        },
        getCarSuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.data = action.payload.data.data
            state.pageCount = action.payload.data.total
        },
        getCarByIdSuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.oneCar = action.payload.data
        },
        addCarSuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        },
        updateCarSuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        },
        deleteCarSuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        }
    }
})

export const getCar = (params) => apiCall({
    url: apiCar,
    method: 'get',
    params: params,
    onStart: car.actions.onGetStart.type,
    onSuccess: car.actions.getCarSuccess.type,
    onFail: car.actions.onFail.type
})

export const getCarById = (id) => apiCall({
    url: apiCar + '/' + id,
    method: 'get',
    onStart: car.actions.onGetStart.type,
    onSuccess: car.actions.getCarByIdSuccess.type,
    onFail: car.actions.onFail.type
})

export const addCar = (data) => apiCall({
    url: apiCar,
    method: 'post',
    data,
    onStart: car.actions.onAddStart.type,
    onSuccess: car.actions.addCarSuccess.type,
    onFail: car.actions.onFail.type
})

export const updateCar = (data) => apiCall({
    url: apiCar,
    method: 'put',
    data,
    onStart: car.actions.onAddStart.type,
    onSuccess: car.actions.updateCarSuccess.type,
    onFail: car.actions.onFail.type
})
export const deleteCar = (id) => apiCall({
    url: apiCar + "/" + id,
    method: 'delete',
    onStart: car.actions.onGetStart.type,
    onSuccess: car.actions.deleteCarSuccess.type,
    onFail: car.actions.onFail.type
})

export default car.reducer
