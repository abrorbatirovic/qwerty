import React, {useEffect, useState} from 'react';
import vector from "../category/Vector.svg";
import {Link, useLocation, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getCarById} from "../../store/car/car";
import './main.css';
import yon from "../../img/car/yon.png";
import malibu from "../../img/car/malibu.png";
import frame from "./Frame.png";
import home from "./home-fill 1.png";
import right from "./right.png";
import left from "./left.png";
import salon from '../../img/123.jpg'
import {Pannellum} from "pannellum-react";

const Car = () => {

    const dispatch = useDispatch();
    const location = useLocation();
    const {id} = useParams();

    const [checked, setChecked] = useState(true);

    const {oneCar, isLoading} = useSelector(state => state.car)

    useEffect(() => {
        dispatch(getCarById(id))
    }, [])

    return (
        <div className={'home'}>

                <Link to={'/login'}>
                    <div className={'login-btn'}>Login </div>
                </Link>

            <div className={'path'}>
                Bosh sahifa <img src={vector} alt={vector} className={'vector'}/> modellari <img src={vector}
                                                                                                 alt={vector}
                                                                                                 className={'vector'}/> {location.state} turlari
            </div>
            <p className={'heading'}>Modellari</p>
            <div className="car-row">
                <div className={'card-left'}>
                    <h5 className={'car-heading'}>{location?.state?.toUpperCase()} MALIBU</h5>
                    <p style={{margin: "8px 0"}}>{oneCar.price} dan</p>
                    {isLoading ? "Loading..." :  <img src={oneCar.imgUrl ? `https://cartestwebapp.herokuapp.com/${oneCar.imgUrl}` :yon} alt="" style={{width: "100%",}}/>}
                    <span>
                        <h4>Marka: </h4>ᅠ<p> {oneCar?.marka?.name}</p>
                    </span>
                    <span>
                        <h4>Tanirovka: </h4>ᅠ<p> {oneCar?.tonirovka}</p>
                    </span>
                    <span>
                        <h4>Motor: </h4>ᅠ<p> {oneCar?.motor}</p>
                    </span>
                    <span>
                        <h4>Year: </h4>ᅠ<p> {oneCar?.year}</p>
                    </span>
                    <span>
                        <h4>Color: </h4>ᅠ<p> {oneCar?.color}</p>
                    </span>
                    <span>
                        <h4>Distance: </h4>ᅠ<p> {oneCar?.distance}</p>
                    </span>
                    <span>
                        <h4>Gearbook: </h4>ᅠ<p> {oneCar?.gearbok}</p>
                    </span>
                    <span>
                        <h4>Description: </h4>ᅠ<p> {oneCar?.description}</p>
                    </span>
                    <span style={{borderBottom: "1px solid rgba(0, 0, 0, 0.2)", width: "100%"}}/>
                    <span>
                        <h4>Umumiy xarajat: </h4>ᅠ<p> {oneCar?.price} so'm dan</p>
                    </span>
                </div>
                <div className={"card-right"}>
                    <div style={{display: "flex", justifyContent: "space-between"}}>
                        <div>
                            <h5 className={'car-heading'}>{location?.state?.toUpperCase()} MALIBU</h5>
                        </div>
                        <div style={{display: "flex"}}>
                            <div>
                                <img src={frame} alt=""/>
                            </div>
                            <div>
                                <img src={home} alt=""/>
                            </div>
                        </div>
                    </div>
                    {
                        checked ? (isLoading ? "Loading..." : <img src={oneCar.imgUrlAutside ? `https://cartestwebapp.herokuapp.com/${oneCar.imgUrlAutside}` : malibu} alt="No img" style={{width: "100%"}}/>)
                            :  <Pannellum
                            width="100%"
                            image={salon}
                            pitch={10}
                            yaw={180}
                            hfov={110}
                            autoLoad
                            showZoomCtrl={false}
                            showFullscreenCtrl={false}
                        />
                    }
                    <div style={{textAlign: "center"}}>


                        <img src={left} alt=""/>

                        360 <sup>o</sup>
                        <img src={right} alt=""/>
                        <p style={{fontSize: "16px", lineHeight: "16px", fontFamily: "Noto Sans, sans-serif"}}>Tasvir
                            tanlangan konfiguratsiyaga mos kelmasligi mumkin. Mashinaning rangi ushbu saytda taqdim
                            etilganidan farq qilishi mumkin.</p>
                        <div style={{marginTop: "40px", display: "flex", justifyContent: "center"}}>
                            <span onClick={() => setChecked(true)} style={{display: "flex"}}>
                                <div>
                                    <input type="radio" checked={checked}/>
                                </div>
                                <div>
                                    <label style={{marginRight: "48px"}}>Tashqi</label>
                                </div>
                            </span>
                            <span onClick={() => setChecked(false)} style={{display: "flex"}}>
                                <div>
                                    <input type="radio" checked={!checked}/>
                                </div>
                                <div>
                                    <label>Ichki makon</label>
                                </div>

                            </span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    );
};

export default Car;